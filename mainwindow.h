#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>

#include "bookdb.h"

namespace Ui {
class MainWindow;
}

class QTableWidgetItem;

class MainWindow final : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow();

    void showEvent(QShowEvent*);
    ~MainWindow();

private:
    std::unique_ptr<Ui::MainWindow> ui;
    std::unique_ptr<BookDB> mDB;
    void fillEntryTable(BookDB::EntryList&&);
    bool updateLastFilters();
    /// If the table is resized, the extra widgets are stored here.
    std::vector<QTableWidgetItem*> mWidgetCache;
    /// Creates or fetches a widget item from the cache.
    inline QTableWidgetItem* genItem(StringRef text);
    inline QTableWidgetItem* genItem(uint64_t text);

    std::string mLastAuthorFilter;
    std::string mLastTitleFilter;
    std::string mLastSubjectFilter;
    std::string mLastLocFilter;
    std::string mLastBackFilter;
    std::string mLastKeyFilter;

public slots:
    void refreshEntryList();
    void cellChanged(int, int);
    void filterChanged();
    void searchFilterChanged();
    void removeEntry();
    void addEntry();
    void exportEntries();
    void runCustomQuery();
};

#endif // MAINWINDOW_H
