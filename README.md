# README #

This is a simple Book Database that I wrote using SQLite3 and Qt. It's not meant to be fast or feature complete. It works for what I need, which is all I wanted.

Note that the sqlite3 amalgamated file is contained within the project, but it is unchanged from the one you can download yourself off the sqlite3 website.

Everything else is GPL3 licensed.
