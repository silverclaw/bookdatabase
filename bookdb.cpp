#include "bookdb.h"
#include "csv.h"

namespace
{
const char DBSchema[] =
        "CREATE TABLE Books ("
        "id integer NOT NULL PRIMARY KEY,"
        "author TEXT, title TEXT, subject TEXT, location TEXT, back TEXT, keywords TEXT)";
void errorLogCallback(void*, int iErrCode, const char* zMsg){
  fprintf(stderr, "(%d) %s\n", iErrCode, zMsg);
}
}

BookDB::Statement::Statement(sqlite3* parent, const char* stmt)
{
    const int result = sqlite3_prepare_v2(parent, stmt, -1, &mStatement, nullptr);
    if (result != SQLITE_OK) throw StatementPrepareFail();
    mArgCount = sqlite3_bind_parameter_count(mStatement);
}

BookDB::Statement::~Statement()
{
    sqlite3_finalize(mStatement);
}

void BookDB::Statement::bind(std::size_t index, StringRef str)
{
    const int result = sqlite3_bind_text(mStatement, index+1, str, str.size(), SQLITE_TRANSIENT);
    if (result != SQLITE_OK) throw StatementBindFail();
}

void BookDB::Statement::bind(std::size_t index, uint64_t v)
{
   const int result =  sqlite3_bind_int(mStatement, index+1, v);
   if (result != SQLITE_OK) throw StatementBindFail();
}

void BookDB::Statement::reset()
{
    const int result = sqlite3_reset(mStatement);
    if (result != SQLITE_OK) throw StatementResetFail();
}

bool BookDB::Statement::next()
{
    const int result = sqlite3_step(mStatement);
    if (result == SQLITE_ROW) return true;
    if (result == SQLITE_DONE) return false;
    throw StatementError();
}

const char* BookDB::Statement::getStr(std::size_t index)
{
    return reinterpret_cast<const char*>(sqlite3_column_text(mStatement, index));
}

uint64_t BookDB::Statement::getInt(std::size_t index)
{
    return sqlite3_column_int(mStatement, index);
}

BookDB::Connection::Connection()
{
    const int rc = sqlite3_open_v2("BookDB.db", &mDB, SQLITE_OPEN_READWRITE, nullptr);
    if (rc == SQLITE_LOCKED) throw DataBaseLocked();
    if (rc != SQLITE_OK) throw NoDataBase();
}

BookDB::Connection::~Connection()
{
    sqlite3_close(mDB);
}

void BookDB::insert(const Entry& E)
{
    mInsert.reset();
    mInsert.bind(0, E.author);
    mInsert.bind(1, E.title);
    mInsert.bind(2, E.subject);
    mInsert.bind(3, E.location);
    mInsert.bind(4, E.back);
    mInsert.bind(5, E.keywords);
    mInsert.next();
}

std::unique_ptr<BookDB> BookDB::FromCSV(const char* filename)
{
    sqlite3_config(SQLITE_CONFIG_LOG, errorLogCallback, nullptr);
    sqlite3* connection;
    const auto mode = SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE;
    const int rc = sqlite3_open_v2("BookDB.db", &connection, mode, nullptr);
    if (rc != SQLITE_OK) throw DataBaseCreateFail();

    BookDB::Transaction transaction(connection);
    transaction.begin();
    {
        Statement createTable(connection, DBSchema);
        createTable.next();
    }
    transaction.commit();

    // Now that the main table has been created, we can create the BookDB instance.
    std::unique_ptr<BookDB> db(new BookDB(connection));
    // Author,Title,Subject,Location,HB/PB,Keywords
    io::CSVReader<6, io::trim_chars<' ', '\t'>, io::double_quote_escape<',', '"'> > in(filename);
    in.read_header(io::ignore_extra_column, "Author", "Title", "Subject", "Location", "HB/PB", "Keywords");
    char* author;
    char* title;
    char* subject;
    char* location;
    char* back;
    char* keywords;
    transaction.begin();
    while (in.read_row(author, title, subject, location, back, keywords))
    {
        const std::size_t authorLen = std::strlen(author);
        const std::size_t titleLen = std::strlen(title);
        const std::size_t subjectLen = std::strlen(subject);
        const std::size_t locationLen = std::strlen(location);
        const std::size_t backLen = std::strlen(back);
        const std::size_t keywordsLen = std::strlen(keywords);

        // Don't care about the empty entry that comes after the headers.
        if ((authorLen | titleLen | subjectLen | locationLen | backLen | keywordsLen) == 0) continue;
        BookDB::Entry E;
        E.author = author;
        E.title = title;
        E.subject = subject;
        E.location = location;
        E.back = back;
        E.keywords = keywords;
        db->insert(E);
    }
    transaction.commit();

    return db;
}


BookDB::~BookDB()
{

}

BookDB::EntryList::operator BookDB::Entry() const noexcept
{
    Entry entry;
    entry.id = mStatement.getInt(0);
    entry.author = mStatement.getStr(1);
    entry.title = mStatement.getStr(2);
    entry.subject = mStatement.getStr(3);
    entry.location = mStatement.getStr(4);
    entry.back = mStatement.getStr(5);
    entry.keywords = mStatement.getStr(6);
    return entry;
}

BookDB::EntryList BookDB::getEntry(uint64_t id)
{
    mGetID.reset();
    mGetID.bind(0, id);
    return EntryList(mGetID);
}

namespace
{
bool AppendQuery(std::string& query, StringRef element, const StringRef& filter, bool needsAnd)
{
    if (filter.size() != 0)
    {
        if (needsAnd) query.append(" AND ");
        //instr(column, 'cats')
        query.append("(instr(UPPER(");
        query.append(std::string(element, element.size()));
        query.append("),UPPER(?)) > 0)");
        //query.append(" LIKE ?");
        return true;
    }
    return false;
}
}

BookDB::EntryList BookDB::getFilteredEntries(const Entry& filter)
{
    // There are too many filter combinations, so we need to build a statement here.
    std::string query;
    query.reserve(1024);
    query.append("SELECT * FROM Books WHERE ");
    bool needsAnd = false;
    needsAnd |= AppendQuery(query, "author", filter.author, needsAnd);
    needsAnd |= AppendQuery(query, "title", filter.title, needsAnd);
    needsAnd |= AppendQuery(query, "subject", filter.subject, needsAnd);
    needsAnd |= AppendQuery(query, "location", filter.location, needsAnd);
    needsAnd |= AppendQuery(query, "back", filter.back, needsAnd);
    needsAnd |= AppendQuery(query, "keywords", filter.keywords, needsAnd);

    mLastQuery.reset(new Statement(mDB, query.c_str()));
    uint32_t index = 0;
    if (filter.author.size()) mLastQuery->bind(index++, filter.author);
    if (filter.title.size()) mLastQuery->bind(index++, filter.title);
    if (filter.subject.size()) mLastQuery->bind(index++, filter.subject);
    if (filter.location.size()) mLastQuery->bind(index++, filter.location);
    if (filter.back.size()) mLastQuery->bind(index++, filter.back);
    if (filter.keywords.size()) mLastQuery->bind(index++, filter.keywords);
    return EntryList(*mLastQuery);
}

BookDB::EntryList BookDB::getAnyField(StringRef field)
{
    mGetAny.reset();
    mGetAny.bind(0, field);
    mGetAny.bind(1, field);
    mGetAny.bind(2, field);
    mGetAny.bind(3, field);
    mGetAny.bind(4, field);
    mGetAny.bind(5, field);
    return EntryList(mGetAny);
}

void BookDB::RunUpdate(Statement& statement, uint64_t id, StringRef str)
{
    statement.reset();
    statement.bind(0, str);
    statement.bind(1, id);
    statement.next();
}
