#ifndef BOOKDB_H
#define BOOKDB_H

#include <exception>
#include <memory>
#include <cstring>
#include <algorithm>
#include "sqlite3.h"

class BookDBError : public std::exception {};
class StatementError : public BookDBError{};

class NoDataBase final : public BookDBError {};
class DataBaseLocked final : public BookDBError {};
class DataBaseCreateFail final : public BookDBError {};
class StatementPrepareFail final : public StatementError {};
class StatementResetFail final : public StatementError {};
class StatementBindFail final : public StatementError {};

class StringRef
{
public:
    StringRef() = default;

	StringRef(const StringRef& other) noexcept : mData(other.mData), mLen(other.mLen)
    {
	}

	StringRef(const char* data) noexcept : mData(data), mLen(std::strlen(data))
    {
	}

	StringRef(const char* data, size_t len) noexcept : mData(data), mLen(len)
    {
	}

    ~StringRef() = default;

	bool operator==(const StringRef& other) const noexcept
    {
		if (other.mLen != mLen) return false;
		return memcmp(other.mData, mData, mLen) == 0;
	}

    operator const char*() const noexcept
    {
		return mData;
	}

	bool operator<(const StringRef& other) const noexcept
    {
		const size_t cmp_len = std::min(other.mLen, mLen);
		int result = strncmp(mData, other.mData, cmp_len);
		if (result < 0) return true;
		if (result > 0) return false;
		// The smaller string goes first
		return mLen < other.mLen;
	}

	bool operator>(const StringRef& other) const noexcept
    {
		const size_t cmp_len = std::min(other.mLen, mLen);
		int result = strncmp(mData, other.mData, cmp_len);
		if (result < 0) return false;
		if (result > 0) return true;
		// The smaller string goes first
		return mLen > other.mLen;
	}

	size_t size() const noexcept
    {
		return mLen;
	}

private:
	const char* mData = "";
	size_t mLen = 0;
};


class BookDB final
{
    class Connection
    {
    public:
        Connection();
        explicit Connection(sqlite3* db) noexcept : mDB(db) {}
        ~Connection();
        operator sqlite3*() noexcept { return mDB; }
    private:
        sqlite3* mDB;
    };

    class Statement
    {
        Statement() = delete;
        Statement(const Statement&) = delete;
        Statement(Statement&&) = delete;
        Statement& operator=(const Statement&) = delete;
        Statement& operator=(Statement&&) = delete;
    public:
        Statement(sqlite3* parent, const char* stmt);
        ~Statement();
        void reset();
        /// Advances to the next element in the query. Returns false if there is no next element.
        bool next();
        void unbind(std::size_t index);
        void bind(std::size_t index, StringRef);
        void bind(std::size_t index, uint64_t);
        const char* getStr(std::size_t index);
        uint64_t getInt(std::size_t index);
    private:
        sqlite3_stmt* mStatement;
        std::size_t mArgCount;
    };

    class Transaction
    {
    public:
        explicit Transaction(sqlite3* db) :
            mStart(db, "BEGIN"), mEnd(db, "END"), mCommit(db, "COMMIT"), mCancel(db, "ROLLBACK")
        {}

        void begin()
        {
            if (mActive) cancel();
            mStart.next();
            mActive = true;
        }

        void commit()
        {
            if (!mActive) throw StatementError();
            mCommit.next();
            mActive = false;
        }

        void cancel()
        {
            if (!mActive) throw StatementError();
            mCancel.next();
            mActive = false;
        }

        ~Transaction()
        {
            if (mActive) cancel();
        }

    private:
        bool mActive = false;
        Statement mStart;
        Statement mEnd;
        Statement mCommit;
        Statement mCancel;
    };

    explicit BookDB(sqlite3* db) : mDB(db) {}
public:
    struct Entry
    {
        uint64_t id;
        StringRef author;
        StringRef title;
        StringRef subject;
        StringRef location;
        StringRef back;
        StringRef keywords;
    };

    class EntryList
    {
        EntryList(const EntryList&) = delete;
    public:
        EntryList(EntryList&&) = default;
        explicit EntryList(Statement& query) : mStatement(query), mHasNext(query.next()) {}
        ~EntryList() = default;

        /// Current element.
        operator Entry() const noexcept;
        /// Keeps returning true if there are more elements to process.
        operator bool() const noexcept { return mHasNext; }
        /// Advance.
        EntryList& operator++() { mHasNext = mStatement.next(); return *this; }

    private:
        Statement& mStatement;
        bool mHasNext;
    };

    BookDB() = default;
    BookDB(const BookDB&) = delete;
    BookDB(BookDB&&) = delete;
    ~BookDB();

    static std::unique_ptr<BookDB> FromCSV(const char*);

    void insert(const Entry&);

    /// Retrieves the entry with this ID.
    EntryList getEntry(uint64_t id);

    void addEntry(const Entry& e)
    {
        mInsert.reset();
        mInsert.bind(0, e.author);
        mInsert.bind(1, e.title);
        mInsert.bind(2, e.subject);
        mInsert.bind(3, e.location);
        mInsert.bind(4, e.back);
        mInsert.bind(5, e.keywords);
        mInsert.next();
    }
    void deleteEntry(uint64_t id) {mDelete.reset(); mDelete.bind(0, id); mDelete.next();}

    /// Gets /all/ unfiltered entries.
    EntryList getAllEntries() { mGetAll.reset(); return EntryList(mGetAll); }
    EntryList getFilteredEntries(const Entry& filter);
    EntryList getAnyField(StringRef field);

    void beginChange() { mTransaction.begin(); }
    void commitChange() { mTransaction.commit(); }
    void discardChange() { mTransaction.cancel(); }

    void changeAuthor(uint64_t id, StringRef author) { RunUpdate(mSetAuthor, id, author); }
    void changeTitle(uint64_t id, StringRef title) { RunUpdate(mSetTitle, id, title); }
    void changeSubject(uint64_t id, StringRef sub) { RunUpdate(mSetSubject, id, sub); }
    void changeLocation(uint64_t id, StringRef loc) { RunUpdate(mSetLocation, id, loc); }
    void changeBack(uint64_t id, StringRef back) { RunUpdate(mSetBack, id, back); }
    void changeKeywords(uint64_t id, StringRef key) { RunUpdate(mSetKeywords, id, key); }

    EntryList runRawSQL(StringRef query)
    {
        mLastQuery.reset(new Statement(mDB, query));
        return EntryList(*mLastQuery);
    }

private:
    static void RunUpdate(Statement&, uint64_t, StringRef);

    Connection mDB;
    Transaction mTransaction{mDB};
    Statement mInsert{mDB, "INSERT INTO Books(author, title, subject, location, back, keywords) "
                           "VALUES(?, ?, ?, ?, ?, ?)"};
    /// Retrieves all entries.
    Statement mGetAll{mDB, "SELECT * FROM Books"};
    /// Retries a specific ID
    Statement mGetID{mDB, "SELECT * FROM Books WHERE id = ?"};
    /// Global search on all columns
    Statement mGetAny{mDB, "SELECT * FROM Books WHERE "
                           "(instr(UPPER(author),UPPER(?)) > 0) OR "
                           "(instr(UPPER(title),UPPER(?)) > 0) OR "
                           "(instr(UPPER(subject),UPPER(?)) > 0) OR "
                           "(instr(UPPER(location),UPPER(?)) > 0) OR "
                           "(instr(UPPER(back),UPPER(?)) > 0) OR "
                           "(instr(UPPER(keywords),UPPER(?)) > 0)"};

    /// Deletes an emtry
    Statement mDelete{mDB, "DELETE FROM Books WHERE id = ?"};

    Statement mSetAuthor{mDB, "UPDATE Books SET author = ? WHERE id = ?"};
    Statement mSetTitle{mDB, "UPDATE Books SET title = ? WHERE id = ?"};
    Statement mSetSubject{mDB, "UPDATE Books SET subject = ? WHERE id = ?"};
    Statement mSetLocation{mDB, "UPDATE Books SET location = ? WHERE id = ?"};
    Statement mSetBack{mDB, "UPDATE Books SET back = ? WHERE id = ?"};
    Statement mSetKeywords{mDB, "UPDATE Books SET keywords = ? WHERE id = ?"};

    // Because "FilteredEntries" returns a complete EntryList object
    // which uses a Statement&, we need to keep the statement alive
    // until the EntryList is processed. Since we are running on a single-threaded way
    // (from the POV of the Qt client), we can make the BookDB own the last filtered query made.
    std::unique_ptr<Statement> mLastQuery;
};

#endif // BOOKDB_H
