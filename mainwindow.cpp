#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QTextStream>
#include <QFileInfo>
#include <QFileDialog>
#include <QTableWidget>
#include "csv.h"
#include "bookdb.h"

namespace
{

namespace ColData
{
    const uint32_t Author = 0;
    const uint32_t Title = 1;
    const uint32_t Subject = 2;
    const uint32_t Location = 3;
    const uint32_t Back = 4;
    const uint32_t Keywords = 5;
    const uint32_t ID = 6;
    const uint32_t Count = 7;
}

QTableWidgetItem* GenItem(StringRef text, bool makeEditable = true)
{
    QTableWidgetItem* item = new QTableWidgetItem();
    item->setText(QString::fromUtf8(text));
    if (makeEditable)
    {
        item->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    }
    return item;
}

QTableWidgetItem* GenItem(uint64_t num)
{
    QTableWidgetItem* item = new QTableWidgetItem();
    item->setText(QString::number(num, 10));
    return item;
}

StringRef GetStringRef(std::string& str)
{
    if (str.size() == 0) StringRef("", 0);
    return StringRef(str.c_str(), str.size());
}
}

MainWindow::MainWindow() :
    QMainWindow(nullptr), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QTableWidget* table = ui->entryTable;
    table->setColumnCount(ColData::Count);
    table->setColumnHidden(ColData::ID, true);
    table->setHorizontalHeaderItem(ColData::Author, GenItem("Author", false));
    table->setHorizontalHeaderItem(ColData::Title, GenItem("Title", false));
    table->setHorizontalHeaderItem(ColData::Subject, GenItem("Subject", false));
    table->setHorizontalHeaderItem(ColData::Location, GenItem("Location", false));
    table->setHorizontalHeaderItem(ColData::Back, GenItem("Back", false));
    table->setHorizontalHeaderItem(ColData::Keywords, GenItem("Keywords", false));

    // Replaced from editingFinished() to textEdited(QString)
    // could probably be made faster if we direct it to per-filter setting.
    connect(ui->authorFilter, SIGNAL(textEdited(QString)), this, SLOT(filterChanged()));
    connect(ui->titleFilter, SIGNAL(textEdited(QString)), this, SLOT(filterChanged()));
    connect(ui->subjectFilter, SIGNAL(textEdited(QString)), this, SLOT(filterChanged()));
    connect(ui->locationFilter, SIGNAL(textEdited(QString)), this, SLOT(filterChanged()));
    connect(ui->backFilter, SIGNAL(textEdited(QString)), this, SLOT(filterChanged()));
    connect(ui->keywordsFilter, SIGNAL(textEdited(QString)), this, SLOT(filterChanged()));
    connect(ui->searchFilter, SIGNAL(textEdited(QString)), this, SLOT(searchFilterChanged()));

    connect(ui->delButton, SIGNAL(clicked()), this, SLOT(removeEntry()));
    connect(ui->addButton, SIGNAL(clicked()), this, SLOT(addEntry()));
    connect(ui->exportButton, SIGNAL(clicked()), this, SLOT(exportEntries()));
    connect(ui->queryGo, SIGNAL(clicked()), this, SLOT(runCustomQuery()));
    connect(ui->refreshButton, SIGNAL(clicked()), this, SLOT(refreshEntryList()));
    connect(ui->queryText, SIGNAL(returnPressed()), this, SLOT(runCustomQuery()));
}

QTableWidgetItem* MainWindow::genItem(StringRef text)
{
    QTableWidgetItem* item;
    if (mWidgetCache.empty()) item = new QTableWidgetItem();
    else
    {
        item = mWidgetCache.back();
        mWidgetCache.pop_back();
    }

    item->setText(QString::fromUtf8(text));
    item->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    return item;
}

QTableWidgetItem* MainWindow::genItem(uint64_t num)
{
    QTableWidgetItem* item;
    if (mWidgetCache.empty()) item = new QTableWidgetItem();
    else
    {
        item = mWidgetCache.back();
        mWidgetCache.pop_back();
    }

    item->setText(QString::number(num, 10));
    return item;
}

void MainWindow::fillEntryTable(BookDB::EntryList&& list)
{
    uint32_t numEntries = 0;
    uint32_t numRows = 100;
    QTableWidget* table = ui->entryTable;
    table->blockSignals(true);

    table->clearContents();
    table->setRowCount(numRows);
    table->setSortingEnabled(false);

    for (; list; ++list, numEntries++)
    {
        if (numEntries == numRows)
        {
            numRows += 100;
            table->setRowCount(numRows);
        }
        BookDB::Entry e = list;
        table->setItem(numEntries, ColData::Author, genItem(e.author));
        table->setItem(numEntries, ColData::Title, genItem(e.title));
        table->setItem(numEntries, ColData::Subject, genItem(e.subject));
        table->setItem(numEntries, ColData::Location, genItem(e.location));
        table->setItem(numEntries, ColData::Back, genItem(e.back));
        table->setItem(numEntries, ColData::Keywords, genItem(e.keywords));
        table->setItem(numEntries, ColData::ID, genItem(e.id));
    }

    // Remove extra widgets from the table into the cache
    mWidgetCache.reserve(ColData::Count * (table->rowCount() - numEntries));
    for (int extra = numEntries; extra < table->rowCount(); ++extra)
    {
        // No more widgets to cache?
        if (table->item(extra, 0) == nullptr) break;
        for (int c = 0; c < ColData::Count; ++c)
        {
            mWidgetCache.push_back(table->takeItem(extra, c));
        }
    }

    table->setRowCount(numEntries);

    table->resizeRowsToContents();
    table->setSortingEnabled(true);
    table->blockSignals(false);

    QString entriesStr("Number of matches: ");
    entriesStr.append(QString::number(numEntries));
    ui->numEntriesLabel->setText(entriesStr);
    return;
}

void MainWindow::removeEntry()
{
    auto* table =  ui->entryTable;
    auto ranges = table->selectedRanges();
    if (ranges.size() != 1) return;
    auto items = ranges[0];
    const auto row = items.topRow();

    QMessageBox msgBox;
    msgBox.setText("Delete this entry?");
    msgBox.setInformativeText(table->item(row, ColData::Title)->text());
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    auto clicked = msgBox.exec();
    if (clicked == QMessageBox::Yes)
    {
        mDB->beginChange();
        mDB->deleteEntry(table->item(row, ColData::ID)->text().toULong());
        mDB->commitChange();
        refreshEntryList();
    }
}

void MainWindow::addEntry()
{
    BookDB::Entry entry;
    std::string author = ui->authorEntry->text().toStdString();
    std::string title = ui->titleEntry->text().toStdString();
    std::string subject = ui->subjectEntry->text().toStdString();
    std::string location = ui->locationEntry->text().toStdString();
    std::string back = ui->backEntry->text().toStdString();
    std::string keywords = ui->keywordEntry->text().toStdString();
    entry.author = GetStringRef(author);
    entry.title = GetStringRef(title);
    entry.subject = GetStringRef(subject);
    entry.location = GetStringRef(location);
    entry.back = GetStringRef(back);
    entry.keywords = GetStringRef(keywords);
    mDB->beginChange();
    mDB->addEntry(entry);
    mDB->commitChange();

    fillEntryTable(mDB->getFilteredEntries(entry));
}

void MainWindow::exportEntries()
{
    const auto* table = ui->entryTable;
    if (table->rowCount() == 0)
    {
        QMessageBox msgBox;
        msgBox.setText("The result list is empty - nothing to export.");
        msgBox.exec();
        return;
    }

    QFileDialog diag(this, "Select CSV");
    diag.setNameFilter("CSV Files (*.csv)");
    diag.setAcceptMode(QFileDialog::AcceptSave);
    diag.setDefaultSuffix("csv");
    diag.exec();
    if (diag.selectedFiles().size() == 0) return;
    QString fileName = diag.selectedFiles().first();
    if (fileName.isEmpty() || fileName.isNull()) return;
    QFileInfo info(fileName);
    if (info.isDir()) return;
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox msgBox;
        msgBox.setText("Couldn't open the file for writing.");
        msgBox.setInformativeText(fileName);
        msgBox.exec();
        return;
    }

    QTextStream out(&file);
    out << "Author,Title,Subject,Location,HB/PB,Keywords\n,,,,,\n";
    for (int row = 0; row < table->rowCount(); ++row)
    {
        for (unsigned col = 0; col < ColData::ID; ++col)
        {
            out << '"' << table->item(row, col)->text() << '"';
            if (col != (ColData::ID-1)) out << ',';
        }
        out << '\n';
    }
}

void MainWindow::cellChanged(int row, int col)
{
    QTableWidget* table = ui->entryTable;

    printf("Cell changed: %d %d\n", row, col);
    // Find the ID of this row.
    uint64_t id = table->item(row, ColData::ID)->text().toULong();
    QString str = table->item(row, col)->text();
    StringRef data(str.toUtf8(), str.size());
    mDB->beginChange();
    switch (col)
    {
        case ColData::Author: mDB->changeAuthor(id, data); break;
        case ColData::Title: mDB->changeTitle(id, data); break;
        case ColData::Subject: mDB->changeSubject(id, data); break;
        case ColData::Location: mDB->changeLocation(id, data); break;
        case ColData::Back: mDB->changeBack(id, data); break;
        case ColData::Keywords: mDB->changeKeywords(id, data); break;
    }
    mDB->commitChange();
}

bool MainWindow::updateLastFilters()
{
    std::string newAuthor = ui->authorFilter->text().toStdString();
    std::string newTitle = ui->titleFilter->text().toStdString();
    std::string newSubject = ui->subjectFilter->text().toStdString();
    std::string newLoc = ui->locationFilter->text().toStdString();
    std::string newBack = ui->backFilter->text().toStdString();
    std::string newKey = ui->keywordsFilter->text().toStdString();

    bool anyChanges = false;
    if (newAuthor != mLastAuthorFilter)
    {
        mLastAuthorFilter = std::move(newAuthor);
        anyChanges = true;
    }
    if (newTitle != mLastTitleFilter)
    {
        mLastTitleFilter = std::move(newTitle);
        anyChanges = true;
    }
    if (newSubject != mLastSubjectFilter)
    {
        mLastSubjectFilter = std::move(newSubject);
        anyChanges = true;
    }
    if (newLoc != mLastLocFilter)
    {
        mLastLocFilter = std::move(newLoc);
        anyChanges = true;
    }
    if (newBack != mLastBackFilter)
    {
        mLastBackFilter = std::move(newBack);
        anyChanges = true;
    }
    if (newKey != mLastKeyFilter)
    {
        mLastKeyFilter = std::move(newKey);
        anyChanges = true;
    }

    return anyChanges;
}

void MainWindow::refreshEntryList()
{
    // Check if all filters are empty.
    if (mLastAuthorFilter.size() == 0 &&
        mLastTitleFilter.size() == 0 &&
        mLastSubjectFilter.size() == 0 &&
        mLastLocFilter.size() == 0 &&
        mLastBackFilter.size() == 0 &&
        mLastKeyFilter.size() == 0)
    {
        fillEntryTable(mDB->getAllEntries());
        return;
    }

    BookDB::Entry filter;
    filter.author = GetStringRef(mLastAuthorFilter);
    filter.title = GetStringRef(mLastTitleFilter);
    filter.subject = GetStringRef(mLastSubjectFilter);
    filter.location = GetStringRef(mLastLocFilter);
    filter.back = GetStringRef(mLastBackFilter);
    filter.keywords = GetStringRef(mLastKeyFilter);

    fillEntryTable(mDB->getFilteredEntries(filter));
}

void MainWindow::filterChanged()
{
    // Check if any of the filters changed.
    const bool anyChanges = updateLastFilters();
    if (!anyChanges) return;

   refreshEntryList();
}

void MainWindow::searchFilterChanged()
{
    std::string filter(ui->searchFilter->text().toStdString());
    if (filter.empty())
    {
        fillEntryTable(mDB->getAllEntries());
        return;
    }
    fillEntryTable(mDB->getAnyField(GetStringRef(filter)));
}

void MainWindow::showEvent(QShowEvent*)
{
    // If a database already exists, then the view
    // has already been populated. Bail out.
    if (mDB) return;
    try
    {
        mDB.reset(new BookDB());
        fillEntryTable(mDB->getAllEntries());
        ui->entryTable->resizeColumnsToContents();
        ui->entryTable->resizeRowsToContents();
    }
    catch (const DataBaseLocked&)
    {
        QMessageBox msgBox;
        msgBox.setText("It appears the database is already opened (it is locked).");
        msgBox.exec();
        return;
    }
    catch (const NoDataBase&)
    {
        QMessageBox msgBox;
        msgBox.setText("Looks like the BooksDB.db file is missing...");
        msgBox.setInformativeText("Do you want to create a new one?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        auto clicked = msgBox.exec();
        if (clicked == QMessageBox::Yes)
        {
            try
            {
                QString fileName = QFileDialog::getOpenFileName(nullptr, "Select CSV", NULL, "CSV Files (*.csv)");
                if (fileName.size() == 0) return;
                mDB = std::move(BookDB::FromCSV(fileName.toUtf8()));
                fillEntryTable(mDB->getAllEntries());
                ui->entryTable->resizeColumnsToContents();
                ui->entryTable->resizeRowsToContents();
            }
            catch (const DataBaseCreateFail&)
            {
                QMessageBox msgBox;
                msgBox.setText("Could not create the database file.");
                msgBox.setInformativeText("There may be insufficient space or permissions in the current directory.");
                msgBox.exec();
            }
            catch (const io::error::can_not_open_file&)
            {
                QMessageBox msgBox;
                msgBox.setText("Could not open the file for reading.");
                msgBox.exec();
            }
            catch (const io::error::base&)
            {
                QMessageBox msgBox;
                msgBox.setText("An unknown error has been detected while parsing the CSV file.");
                msgBox.setInformativeText("The CSV file may be malformed or the schema has changed.");
                msgBox.exec();
            }
            catch (const BookDBError&)
            {
                QMessageBox msgBox;
                msgBox.setText("Database error");
                msgBox.setInformativeText("The database did not initialise correctly.");
                msgBox.exec();
            }
        }
    }
}

void MainWindow::runCustomQuery()
{
    std::string query = ui->queryText->text().toStdString();
    try
    {
        fillEntryTable(mDB->runRawSQL(GetStringRef(query)));
    }
    catch (const StatementPrepareFail&)
    {
        QMessageBox msgBox;
        msgBox.setText("The statement did not prepare correctly.");
        msgBox.exec();
    }
    catch (const StatementError&)
    {
        QMessageBox msgBox;
        msgBox.setText("Statement error - could not execute query.");
        msgBox.exec();
    }
}

MainWindow::~MainWindow()
{
    for(QTableWidgetItem* w : mWidgetCache) delete w;
}
